
function flipCards() {
  $('.CardContainer, .GoogleMaps, .StoresResultsItem').velocity("transition.slideUpIn", {
    delay: 0,
    duration: 500,
    stagger: 50,
  });
  document.body.scrollTop = 0;
};

$(document).ready(function() {

// FUNCTIONS

function PrimaryCrateHome() {
  $(".PrimaryCrate").load("PrimaryCrate_Home.php", function() {

  });
};

function PrimaryCrateHistory() {
  $(".PrimaryCrate").load("PrimaryCrate_Home.php", function() {
    $(".ExpiringStars").addClass("Open");
  });
};

function NavSignedIn() {
  $(".Nav").load("GlobalNav_SignedIn.php", function() {});
};

function NavSignedOut() {
  $(".Nav").load("GlobalNav_SignedOut.php", function() {});
};

// INITIAL LOAD

//PrimaryCrateHome();
NavSignedOut();

// $(".SecondaryCrate").load("SecondaryCrate_Feed.php", function() {
//   flipCards();
// });

$(".SecondaryCrate").load("SecondaryCrate_Feed_SignedOut.php", function() {
  flipCards();
});

$(".PrimaryCrate").load("PrimaryCrate_Home_SignedOut.php", function() {});

/// TEMPORARY HOME PAGE IMAGE

$(".PrimaryCrate").addClass("Mondays");

$(document).on('click', "#NavHome, #NavHistory, #NavCards, #NavStores, #NavSignIn", function () {
  $(".PrimaryCrate").removeClass("Mondays");
  $(".PrimaryCrate").removeClass("Tuesdays");
});

$(document).on('click', " #NavCoffee ", function () {
  $(".PrimaryCrate").removeClass("Mondays");
  $(".PrimaryCrate").addClass("Tuesdays");
});

$(document).on('click', " #NavHomeSignedOut ", function () {
  $(".PrimaryCrate").removeClass("Tuesdays");
  $(".PrimaryCrate").addClass("Mondays");
});

// STORES MOBILE VIEW

function StoresMobileView() {
  $(".SecondaryCrate").hide();
  $(".PrimaryCrate").hide();
  $(".StoresMobileViewContainer").show();
  $(".StoresMobileViewContainer").load("StoresMobileView.php", function() {});
};

function UndoStoresMobileView() {
  $(".SecondaryCrate").show();
  $(".PrimaryCrate").show();
  $(".StoresMobileViewContainer").hide();
};

if (document.documentElement.clientWidth < 500) {
  $(document).on('click', "#NavStores", function () {
    StoresMobileView();
  });
}

// NAVIGATION

$(document).on('click', "#NavHome", function () {
  $(".SecondaryCrate").load("SecondaryCrate_Feed.php", function() {
    flipCards();
  });
  $(".ExpiringStars").removeClass("Open");
  PrimaryCrateHome();
  UndoStoresMobileView();
});

$(document).on('click', "#NavHomeSignedOut", function () {
  $(".SecondaryCrate").load("SecondaryCrate_Feed_SignedOut.php", function() {
    flipCards();
  });
  $(".PrimaryCrate").load("PrimaryCrate_Home_SignedOut.php", function() {});
  NavSignedOut();
});

$(document).on('click', "#NavHistory", function () {
  $(".SecondaryCrate").load("SecondaryCrate_History.php", function() {
    flipCards();
  });
  PrimaryCrateHistory();
  UndoStoresMobileView();

});

$(document).on('click', "#NavCards", function () {
  $(".SecondaryCrate").load("SecondaryCrate_Cards.php", function() {
    flipCards();
  });
  PrimaryCrateHome();
  UndoStoresMobileView();
  $(".ExpiringStars").removeClass("Open");
});

$(document).on('click', "#NavSettings", function () {
  $(".SecondaryCrate").load("SecondaryCrate_Settings.php", function() {
    flipCards();
  });
  $(".ExpiringStars").removeClass("Open");
  PrimaryCrateHome();
  UndoStoresMobileView();
});

$(document).on('click', "#NavSignOut", function () {
  $(".SecondaryCrate").load("SecondaryCrate_Feed_SignedOut.php", function() {
    flipCards();
  });
  $(".PrimaryCrate").load("PrimaryCrate_Home_SignedOut.php", function() {});
  NavSignedOut();
  UndoStoresMobileView();
});

// $(document).on('click', "#NavSignIn", function () {
//   $(".SecondaryCrate").load("SecondaryCrate_Feed.php", function() {
//     flipCards();
//   });
//   $(".PrimaryCrate").load("PrimaryCrate_Home.php", function() {});
//   NavSignedIn();
//   UndoStoresMobileView();
// });

$(document).on('click', "#NavSignIn", function () {
  $(".SecondaryCrate").load("SecondaryCrate_SignIn.php", function() {
    flipCards();
  });
  $(".PrimaryCrate").load("PrimaryCrate_SignIn.php", function() {});
});

$(document).on('click', "#NavSignInSubmit", function () {
  $(".SecondaryCrate").load("SecondaryCrate_Feed.php", function() {
    flipCards();
  });
  NavSignedIn();
  $(".PrimaryCrate").load("PrimaryCrate_Home.php", function() {});
});

$(document).on('click', "#NavStores", function () {
  $(".SecondaryCrate").addClass("UnScrollable");
  $(".SecondaryCrate").load("SecondaryCrate_Stores.php", function() {});
  $(".PrimaryCrate").load("SecondaryCrate_StoreResults.php", function() {
    flipCards();
  });
  $(".ExpiringStars").removeClass("Open");
});

// SIGNED OUT NAV

$(document).on('click', "#NavCoffee", function () {
  $(".SecondaryCrate").load("SecondaryCrate_Coffee.php", function() {
    flipCards();
  });
  $(".PrimaryCrate").load("PrimaryCrate_Coffee.php", function() {});
});

// GLOBAL NAV ACTIVE STATE

$(document).on('click', ".PrimaryNav a", function () {
  $(".PrimaryNav a").removeClass("active");
  $(this).addClass("active");
});

// LOAD/UNLOAD TRANSFER BALANCE PANEL
$(document).on('click', ".TransferBalanceButton", function () {
  $( ".CardsPanel.First" ).addClass("Moved");
  $( ".TransferBalancePanel" ).addClass("Moved");
});

$(document).on('click', ".SliderBackward", function () {
  $( ".CardsPanel.First" ).removeClass("Moved");
  $( ".TransferBalancePanel" ).removeClass("Moved");
});

// LOAD/UNLOAD RELOAD PANEL
$(document).on('click', ".ReloadButton", function () {
  $( ".CardsPanel.First" ).addClass("Moved");
  $( ".ReloadPanel").addClass("Moved");
  $( ".CardContainer" ).removeClass('Open');
});

$(document).on('click', ".SliderBackward", function () {
  $( ".CardsPanel.First" ).removeClass("Moved");
  $( ".ReloadPanel" ).removeClass("Moved");
  $( ".CardContainer" ).removeClass('Open');
});

// NOTIFICATIONS

$(document).on('click', ".GetNotification", function () {
  GetNotification();
  var NotificationString = $(this).attr("data-notif");
  $( ".NotificationGlobal .NotificationText" ).html(NotificationString);
});

$(document).on('click', ".NotificationGlobal", function () {
  $(this).removeClass("Active");
});

function GetNotification() {
	$( ".NotificationGlobal" ).addClass('Active');
	setTimeout(function () {
   $( ".NotificationGlobal" ).removeClass('Active');
 }, 3000);
}

// MAKE A TRANSFER

$(document).on('click', ".MakeATransfer", function () {
  $('#PrimaryCardBalance').html("47.43")
  $('#SecondaryCardBalance').html("0.00")
});

// TRANSFER: CHANGE CARD

$(document).on('click', ".TransferFrom", function () {
  $(this).toggleClass("Open")
  $(this).find(".TransferCardExpand").toggleClass("Open")
});

// REMOVE SECONDARY CARD

$(document).on('click', ".RemoveACard", function () {
  $('#SecondaryCard').velocity("transition.flipYOut", {
    duration: 200,
  });
});

// TRANSFER: MAKE CARD PRIMARY

$(document).on('click', ".MakeCardPrimary", function () {
  $("#PrimaryCardName").html("My Card");
  $("#SecondaryCardName").html("&#10003; My Primary Card");
});

// DISCONNECT FACEBOOK

$(document).on('click', ".DisconnectFacebook", function () {
    // $('#FacebookConnect').velocity("transition.flipYOut", {
    //   duration: 200,
    // });
    $(this).html("Connect");
  });

//

// $( "#RemindMeLater" ).click(function() {
//   $( ".AccountDecision" ).hide();
// });

// ACTIVE STATES FOR PRIMARY NAV

// var whereweat = location.pathname;
// $('a[href="' + whereweat + '"]').addClass('active');

// LINKED CARDS

// $(".linked").click(function(){
// 	window.location=$(this).find("a").attr("href");
// 	console.log('clicked');
// 	return false;
// });
//


  $(document).on('change', '.PaymentMethodOptions input', function(e) {
    if($(this).is('#payWithCreditOrDebit')) {
      $('#cardDetails').slideDown('fast');
      console.log('yes');
    } else {
      $('#cardDetails').slideUp('fast');
      console.log('no');
    }
  });

  $(document).on('click', '#editBilling', function(e) {
    $('.editBillingInfo input').prop('disabled', false);
    $(this).hide();
    $('#saveBillingInfo').show().css('opacity', 1);
    $('.editBillingInfo label').show();
  });

  $(document).on('click', '#editCard', function(e) {
    $('.editCardInfo input').prop('disabled', false);
    $(this).hide();
    $('#saveCardInfo').show().css('opacity', 1);
    $('.editCardInfo label').show();
  });

  $(document).on('click', '#saveBillingInfo', function(e) {
    $('.editBillingInfo input').prop('disabled', true);
    $(this).hide();
    $('#editBilling').show().css('opacity', 1);
    $('.inputFields label').hide();
  });

  $(document).on('click', '#saveCardInfo', function(e) {
    $('.editCardInfo input').prop('disabled', true);
    $(this).hide();
    $('#editCard').show().css('opacity', 1);
    $('.inputFields label').hide();
  });
/// history
  $(document).on('click', ".HistoryItem", function (e) {
    e.stopPropagation();
    e.preventDefault();
    $(".HistoryItem.Open").removeClass('Open');
    $(".CardContainer.Open").removeClass('Open');
    $(".CardClose").removeClass('On');

    $(this).addClass("Open");
    $(this).find(".CardClose").addClass("On");
    $(this).parent().addClass("Open");
    $('html, body').animate({
      scrollTop: $(this).parent().offset().top
  }, 100);

  });

  $(document).on('click', ".CardClose", function(e) {
    e.stopPropagation();
    e.preventDefault();
    console.log('CardClose clicked');
    $(".HistoryItem.Open").removeClass('Open');
    $(".CardContainer.Open").removeClass('Open');
    $(".CardClose.On").removeClass('On');
  });

  $(document).on('change', '.AutoReloadOptions .AutoReloadOption input, .SetAmounts input, .PaymentMethodOptions input', function(e) {
    if($('.SetAmounts input:radio').is(':checked') && $('.PaymentMethodOptions input:radio').is(':checked')) {
      $('.ReloadCard').prop('disabled', false);
    } else {
      $('.ReloadCard').prop('disabled', true);
    }
  });

  $(document).on('click', '.ReloadCard', function(e) {
    GetNotification();
    var primaryCurrentBalance = $('#PrimaryCardBalance').text().replace('$', '');
    //console.log(primaryCurrentBalance);
    $('#PrimaryCardBalance').text(parseInt(primaryCurrentBalance) + parseInt($('.SetAmounts input:radio:checked').val()));
    var NotificationString = $(this).attr("data-notif");
    $( ".NotificationGlobal .NotificationText" ).html('$' + $('.SetAmounts input:radio:checked').val() + '.00 has been transfered to your Primary Card (#2453).');
    // setTimeout(function(){
    //    $("html, body").animate({ scrollTop: 0 }, "slow");
    // }, 3200);
  });

});

// LOADING SECONDARY

$(window).on('scroll',function(){
  var dh = $(document).height(),
  wh = $(window).height(),
  st = $(window).scrollTop();
  if (st <= 0) {
    $(this).scrollTop(0);
  } else if (st + wh >= dh) {
    $(this).scrollTop(dh);
  }
});

