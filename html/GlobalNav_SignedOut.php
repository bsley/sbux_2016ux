<header>
  <a class="Logo" id="NavHomeSignedOut"></a>
  <nav class="NavContainer">
  	<ul class="PrimaryNav">
  		<li><a id="NavCoffee">Coffee</a></li>
  		<li><a>Food</a></li>
      <li><a>Drinks</a></li>
      <li><a>Stores</a></li>
  	</ul>
  	<ul class="SecondaryNav">
      <li><a href="http://store.starbucks.com" target="_blank">Shop</a></li>
  		<!-- <li><a href="">Blog</a></li> -->
      <li><a href="">Gift</a></li>
      <li><a href="http://company.starbucks.com/" target="_blank">Company</a></li>
      <li><span>|</span></li>
      <li><a id="NavSignIn">Sign In</a></li>
  	</ul>
  </nav>
</header>

<div class="Search">
	<input placeholder="Search Anything">
</div>