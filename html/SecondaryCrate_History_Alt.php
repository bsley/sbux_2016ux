<!-- 	<div class="CardsPanel First">




	</div>


 -->

<!-- this is the HTML template -->
<script type="text/html" id="template">
    <div class="row">
        {{#Transactions}}

				<div class="CardContainer" id="CheesePizza">
				  <div class="Card HistoryItem Purchase SliderForward">
				    <div class="CardContents">
				    	<p class="SliderBackward CardClose">×</p>
				    	<div class="HistoryCheckInfo">
				    		<div class="HistoryCheckDate">January 4, 2016 at 08:21PM</div>
				 				<div class="HistoryCheckID">Check #797911</div>
				 			</div>
				    	<div class="HistoryIcon"></div>
				      <div class="PurchaseInfo">
				      	<p class="HistoryStarsEarned">{{StarsEarned}} Stars Earned</p>
				      	<p class="HistoryCardNumber">My Card (#4348)</p>
				      	<p class="HistoryLocationName">{{StoreLocation}}</p>
				      </div>
				      <div class="FinancialInfo">
				      	<p>USD $16.32</p>
				      	<p class="HistoryItem SliderForward HistoryDate">{{TransactionDate}}</p>
				      </div>
				      <div class="HistoryBasket receipt">
				      	<div class="HistoryAddress bottom-dotted">
				        	<p>
				        		{{StoreLocation}}<br>
				        		1124 Occidental Ave.<Br>
				        		Seattle, WA 98101
				        	</p>
				      	</div>
								<div class="itemized-list bottom-dotted">
							    <div class="itemized-list-item">
							      <div class="left-column">
							        <p class="item-name">Spanish Coffee</p>
							        <p class="item-size">Brugal Spiced Rum</p>
							        <p class="item-size">2 Shots added</p>
							        <p class="item-size">Bailey's Irish Coffee Creamer
							      </div>
							      <p class="right-column">$14.00</p>
							    </div>
							    <div class="itemized-list-item">
							      <div class="left-column">
							        <p class="item-name">Banana Nut Bread</p>
							        <p class="item-size">Warmed</p>
							      </div>
							      <p class="right-column">$10.75</p>
							    </div>
							    <div class="itemized-list-item">
							      <div class="left-column">
							        <p class="item-name">Grande Caffe Latte</p>
							      </div>
							      <p class="right-column">$5.00</p>
							    </div>
							  </div>
							  <div class="receipt-footer bottom-dotted">
							    <div class="receipt-tax discount">
							      <p class="left-column">Discount</p>
							      <p class="right-column">-$1.81</p>
							    </div>
							    <div class="receipt-tax">
							      <p class="left-column">Tax</p>
							      <p class="right-column">$0.40</p>
							    </div>
							    <div class="towards-stars receipt-tax">
							      <div class="left-column">
							        <p class="gold-text">Total applied toward Stars</p>
							      </div>
							      <div class="right-column">
							        <p class="gold-text">$10.27</p>
							      </div>
							    </div>
							  </div>
							  <div class="order-total">
							    <div class="receipt-tax">
							      <p class="left-column">Order Total</p>
							      <p class="right-column">$16.32</p>
							    </div>
							  </div>
				      </div>
				    </div>
				  </div>
				</div>


      {{/Transactions}}
    </div>
</script>

<script type="text/javascript">

$.getJSON(
  "History.json",
  function(data) {
    // Success! Do stuff with data.
    console.log(data);
    var template = $('#template').html();
    var html = Mustache.to_html(template,data);
    $('#Transactions').html(html);
  }
);
</script>

<!-- Insertion point for handlebars template -->

<section id="Transactions">
  <div class="row" id="output"></div>
</section>

<!-- 	<div class="CardsPanel First">




	</div>


 -->

<!-- this is the HTML template -->
<script type="text/html" id="template">
    <div class="row">
        {{#Transactions}}

				<div class="CardContainer" id="CheesePizza">
				  <div class="Card HistoryItem Purchase SliderForward">
				    <div class="CardContents">
				    	<p class="SliderBackward CardClose">×</p>
				    	<div class="HistoryCheckInfo">
				    		<div class="HistoryCheckDate">January 4, 2016 at 08:21PM</div>
				 				<div class="HistoryCheckID">Check #797911</div>
				 			</div>
				    	<div class="HistoryIcon"></div>
				      <div class="PurchaseInfo">
				      	<p class="HistoryStarsEarned">{{StarsEarned}} Stars Earned</p>
				      	<p class="HistoryCardNumber">My Card (#4348)</p>
				      	<p class="HistoryLocationName">{{StoreLocation}}</p>
				      </div>
				      <div class="FinancialInfo">
				      	<p>USD $16.32</p>
				      	<p class="HistoryItem SliderForward HistoryDate">{{TransactionDate}}</p>
				      </div>
				      <div class="HistoryBasket receipt">
				      	<div class="HistoryAddress bottom-dotted">
				        	<p>
				        		{{StoreLocation}}<br>
				        		1124 Occidental Ave.<Br>
				        		Seattle, WA 98101
				        	</p>
				      	</div>
								<div class="itemized-list bottom-dotted">
							    <div class="itemized-list-item">
							      <div class="left-column">
							        <p class="item-name">Spanish Coffee</p>
							        <p class="item-size">Brugal Spiced Rum</p>
							        <p class="item-size">2 Shots added</p>
							        <p class="item-size">Bailey's Irish Coffee Creamer
							      </div>
							      <p class="right-column">$14.00</p>
							    </div>
							    <div class="itemized-list-item">
							      <div class="left-column">
							        <p class="item-name">Banana Nut Bread</p>
							        <p class="item-size">Warmed</p>
							      </div>
							      <p class="right-column">$10.75</p>
							    </div>
							    <div class="itemized-list-item">
							      <div class="left-column">
							        <p class="item-name">Grande Caffe Latte</p>
							      </div>
							      <p class="right-column">$5.00</p>
							    </div>
							  </div>
							  <div class="receipt-footer bottom-dotted">
							    <div class="receipt-tax discount">
							      <p class="left-column">Discount</p>
							      <p class="right-column">-$1.81</p>
							    </div>
							    <div class="receipt-tax">
							      <p class="left-column">Tax</p>
							      <p class="right-column">$0.40</p>
							    </div>
							    <div class="towards-stars receipt-tax">
							      <div class="left-column">
							        <p class="gold-text">Total applied toward Stars</p>
							      </div>
							      <div class="right-column">
							        <p class="gold-text">$10.27</p>
							      </div>
							    </div>
							  </div>
							  <div class="order-total">
							    <div class="receipt-tax">
							      <p class="left-column">Order Total</p>
							      <p class="right-column">$16.32</p>
							    </div>
							  </div>
				      </div>
				    </div>
				  </div>
				</div>


      {{/Transactions}}
    </div>
</script>

<script type="text/javascript">

$.getJSON(	aZX
  "History.json",
  function(data) {
    // Success! Do stuff with data.
    console.log(data);
    var template = $('#template').html();
    var html = Mustache.to_html(template,data);
    $('#Transactions').html(html);
  }
);
</script>

<!-- Insertion point for handlebars template -->

<section id="Transactions">
  <div class="row" id="output"></div>
</section>

<!-- 	<div class="CardsPanel First">




	</div>


 -->

<!-- this is the HTML template -->
<script type="text/html" id="template">
    <div class="row">
        {{#Transactions}}

				<div class="CardContainer" id="CheesePizza">
				  <div class="Card HistoryItem Purchase SliderForward">
				    <div class="CardContents">
				    	<p class="SliderBackward CardClose">×</p>
				    	<div class="HistoryCheckInfo">
				    		<div class="HistoryCheckDate">January 4, 2016 at 08:21PM</div>
				 				<div class="HistoryCheckID">Check #797911</div>
				 			</div>
				    	<div class="HistoryIcon"></div>
				      <div class="PurchaseInfo">
				      	<p class="HistoryStarsEarned">{{StarsEarned}} Stars Earned</p>
				      	<p class="HistoryCardNumber">My Card (#4348)</p>
				      	<p class="HistoryLocationName">{{StoreLocation}}</p>
				      </div>
				      <div class="FinancialInfo">
				      	<p>USD $16.32</p>
				      	<p class="HistoryItem SliderForward HistoryDate">{{TransactionDate}}</p>
				      </div>
				      <div class="HistoryBasket receipt">
				      	<div class="HistoryAddress bottom-dotted">
				        	<p>
				        		{{StoreLocation}}<br>
				        		1124 Occidental Ave.<Br>
				        		Seattle, WA 98101
				        	</p>
				      	</div>
								<div class="itemized-list bottom-dotted">
							    <div class="itemized-list-item">
							      <div class="left-column">
							        <p class="item-name">Spanish Coffee</p>
							        <p class="item-size">Brugal Spiced Rum</p>
							        <p class="item-size">2 Shots added</p>
							        <p class="item-size">Bailey's Irish Coffee Creamer
							      </div>
							      <p class="right-column">$14.00</p>
							    </div>
							    <div class="itemized-list-item">
							      <div class="left-column">
							        <p class="item-name">Banana Nut Bread</p>
							        <p class="item-size">Warmed</p>
							      </div>
							      <p class="right-column">$10.75</p>
							    </div>
							    <div class="itemized-list-item">
							      <div class="left-column">
							        <p class="item-name">Grande Caffe Latte</p>
							      </div>
							      <p class="right-column">$5.00</p>
							    </div>
							  </div>
							  <div class="receipt-footer bottom-dotted">
							    <div class="receipt-tax discount">
							      <p class="left-column">Discount</p>
							      <p class="right-column">-$1.81</p>
							    </div>
							    <div class="receipt-tax">
							      <p class="left-column">Tax</p>
							      <p class="right-column">$0.40</p>
							    </div>
							    <div class="towards-stars receipt-tax">
							      <div class="left-column">
							        <p class="gold-text">Total applied toward Stars</p>
							      </div>
							      <div class="right-column">
							        <p class="gold-text">$10.27</p>
							      </div>
							    </div>
							  </div>
							  <div class="order-total">
							    <div class="receipt-tax">
							      <p class="left-column">Order Total</p>
							      <p class="right-column">$16.32</p>
							    </div>
							  </div>
				      </div>
				    </div>
				  </div>
				</div>


      {{/Transactions}}
    </div>
</script>

<script type="text/javascript">

$.getJSON(	aZX
  "History.json",
  function(data) {
    // Success! Do stuff with data.
    console.log(data);
    var template = $('#template').html();
    var html = Mustache.to_html(template,data);
    $('#Transactions').html(html);
  }
);
</script>

<!-- Insertion point for handlebars template -->

<section id="Transactions">
  <div class="row" id="output"></div>
</section>

