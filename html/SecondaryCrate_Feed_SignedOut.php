  <div class="CardContainer">
    <div class="Card FullBleed" id="JoinRewards">
      <div class="CardContents Negative">
        <h2>Make every sip more rewarding.</h2>
        <p class="MessageDesc HiddenOnMobile">Earn free food and drinks, get free refills, pay and order with your phone, and more.</p><Br>
        <button class="Button Negative">Join Now</button>
      </div>
    </div>
  </div>
  <div class="CardContainer">
    <div class="Card MessageCard Medium">
      <div class="CardContents">
        <div class="MessageThumbnail" style="background-image: url(_img/mustache2.jpg)"></div>
        <div class="MessageDetails">
          <p class="MessageTitle">You're invited to the new Roastery in your neighborhood!</p>
          <p class="MessageDesc">Seattle welcomes a green tea and creamy milk, finished with a dash of lemon your barista next time you visit.</p>
          <a href="http://roastery.starbucks.com/" target="_blank"><button class="Button">Explore</button></a>
        </div>
      </div>
    </div>
  </div>
  <div class="CardContainer">
    <div class="Card FullBleed Large">
      <div class="CardThumbnail" style="background-image: url(_img/papua.jpg)"></div>
      <div class="CardContents">
        <h2>New Papau New Guinea Roast from Starbucks Reserve</h2>
        <p class="MessageDesc">Seattle welcomes a green tea and creamy milk, finished with a dash of lemon.</p>
        <button class="Button">Explore</button>
      </div>
    </div>
  </div>
  <div class="CardContainer">
    <div class="Card FullBleed" id="Card_Chorizo">
      <div class="CardContents Negative">
        <h2>The New Spicy Chorizo Breakfast Sandwich</h2>
        <button class="Button Negative">Learn More</button>
      </div>
    </div>
  </div>


<div class="FooterPlaceholder">
  <p>Footer Content</p>
  <p>© 2016 Starbucks Corporation. All rights reserved.</p>
</div>