<div class="CardContainer">
    <div class="Card SpotifyCard">
      <div class="CardContents ">
        <div class="SpotifyCover"></div>
        <div class="SpotifyDetails">
          <p class="SpotifyTrackTitle">I Should Live In Salt</p>
          <p class="SpotifyArtistTitle">The National</p>
          <p class="SpotifyNowPlaying">Now Playing at Pioneer Square</p>
        </div>
      </div>
    </div>
  </div>
  <div class="CardContainer">
    <div class="Card ReceiptCard">
      <div class="CardContents">
        <div class="ReceiptHeader">Receipt</div>
          <div class="ReceiptDetails">
            <p class="ReceiptTitle">Today at 1:35pm
            <span class="ReceiptAmount">$16.32</span></p>
          </div>
          <div class="ReceiptButton">
            <button class="Button">Details</button>
          </div>
      </div>
    </div>
  </div>
  <div class="CardContainer">
    <div class="Card OfferCard">
      <div class="CardContents">
        <div class="OfferHeader">Available Offers</div>
        <div class="OfferIndividual GetNotification" data-notif="To redeem this offer, tell your barista.">
          <p class="OfferTitle">$2 off espresso after 2PM
          <span class="OfferExpDate">Expires in 2 days</span></p>
          <p class="OfferCode">Code: #4563</p>
        </div>
        <div class="OfferIndividual GetNotification" data-notif="To redeem this offer, tell your barista.">
          <p class="OfferTitle">Buy one bagel, get the second one free
          <span class="OfferExpDate">Expires in 2 days</span></p>
          <p class="OfferCode">Code: #4563</p>
        </div>
        <div class="OfferIndividual GetNotification" data-notif="To redeem this offer, tell your barista.">
          <p class="OfferTitle">Free Frappuchino with the purchase of a mug
          <span class="OfferExpDate">Expires in 2 days</span></p>
          <p class="OfferCode">Code: #4563</p>
        </div>
      </div>
    </div>
  </div>
  <div class="CardContainer">
    <div class="Card MessageCard">
      <div class="CardContents">
        <div class="MessageThumbnail">
          <img src="_img/birthday.jpg">
        </div>
        <div class="MessageDetails">
          <p class="MessageTitle">It's your birthday! This one's on us.</p>
          <p class="MessageDesc">Tell your barista next time you pay with your phone that you'd like to redeem your feed food or drink.</p>
          <p class="MessageExpDate">Expires in 2 days</p>
          <!-- <p class="MessageButton">
            <button class="Button">Redeem</button>
          </p> -->
        </div>
      </div>
    </div>
  </div>
  <div class="CardContainer">
    <div class="Card MessageCard Medium">
      <div class="CardContents">
        <div class="MessageThumbnail" style="background-image: url(_img/mustache2.jpg)"></div>
        <div class="MessageDetails">
          <p class="MessageTitle">You're invited to the new Roastery in your neighborhood!</p>
          <p class="MessageDesc">Seattle welcomes a green tea and creamy milk, finished with a dash of lemon your barista next time you visit.</p>
          <a href="http://roastery.starbucks.com/" target="_blank"><button class="Button">Explore</button></a>
        </div>
      </div>
    </div>
  </div>
  <div class="CardContainer">
    <div class="Card MessageCard">
      <div class="CardContents">
        <div class="MessageThumbnail">
          <img src="_img/nyc.jpg">
        </div>
        <div class="MessageDetails">
          <div class="MessageIcon">
          <img src="_img/nyc_icon.jpg">
          </div>
          <p class="MessageTitle">Free App: NYTNow</p>
          <p class="MessageDesc">Your Guide to the News. NYT Now is a free news app from The New York Times. Get the most important stories of the day on your iPhone.</p>
          <p class="MessageExpDate">Expires in 2 days</p>
          <p class="MessageButton">
            <!-- <button class="Button">See All</button>
            <button class="Button">Details</button> -->
            <a href="https://itunes.apple.com/us/app/nyt-now-your-guide-to-the-news/id798993249?mt=8" target="_blank"><button class="Button">Download</button></a>
          </p>
        </div>
      </div>
    </div>
  </div>
  <!-- <div class="CardContainer">
    <div class="Card FullBleed" id="Card_Chorizo">
      <div class="CardContents Negative">
        <h2>The New Spicy Chorizo Breakfast Sandwich</h2>
        <a href="http://www.starbucks.com/menu/food/hot-breakfast/spicy-chorizo-monterey-jack-egg-breakfast-sandwich?foodZone=9999" target="_blank"><button class="Button Negative">Learn More</button></a>
      </div>
    </div>
  </div> -->


<div class="FooterPlaceholder">
  <p>Footer Content</p>
  <p>© 2016 Starbucks Corporation. All rights reserved.</p>
</div>
