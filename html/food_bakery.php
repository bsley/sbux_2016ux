<?php include("header.php"); ?>
<?php include("nav_signedin.php"); ?>

<section class="PrimaryCrate" id="Hero_GreenTea">
  <div class="PrimaryCrateInner">
    <!-- <p>Food > Bakery</p>
    <br><Br> -->
  </div>
</section>
<section class="SecondaryCrate">
  <div class="CardContainer Half">
  <div class="Card linked FullBleed">
    <div class="CardThumbnail" style="background-image: url(http://www.starbucks.com/assets/4581166d0c6b4e2982f52519275ce647.jpg)"></div>
      <div class="CardContents">
      <h2><a href="food_bakery_detail.php">Chonga Bagel</a></h2>
    </div>
  </div>
</div>
<div class="CardContainer Half">
  <div class="Card FullBleed">
    <div class="CardThumbnail" style="background-image: url(http://www.starbucks.com/assets/0189c49ad8e1414eab565f76d2fec849.jpg)"></div>
      <div class="CardContents">
      <h2>8-Grain Roll</h2>
    </div>
  </div>
</div>
<div class="CardContainer Half">
  <div class="Card FullBleed">
    <div class="CardThumbnail" style="background-image: url(http://www.starbucks.com/assets/a675ad4bdd2a4023982a6fbbd86eac09.jpg)"></div>
      <div class="CardContents">
      <h2>Almond Croissant</h2>
    </div>
  </div>
</div>
<div class="CardContainer Half">
  <div class="Card FullBleed">
    <div class="CardThumbnail" style="background-image: url(http://www.starbucks.com/assets/067c69511d4d483aa99207d756c38970.jpg)"></div>
      <div class="CardContents">
      <h2>Banana Nut Bread</h2>
    </div>
  </div>
</div>
<div class="CardContainer Half">
  <div class="Card FullBleed">
    <div class="CardThumbnail" style="background-image: url(http://www.starbucks.com/assets/229a7ecc87f34092bd09c7f06c7c31dc.jpg)"></div>
      <div class="CardContents">
      <h2>Blueberry Muffin with Yogurt and Honey</h2>
    </div>
  </div>
</div>
<div class="CardContainer Half">
  <div class="Card FullBleed">
    <div class="CardThumbnail" style="background-image: url(http://www.starbucks.com/assets/1274f3cef6a14fa0aa2eab7eaf7314ba.jpg)"></div>
      <div class="CardContents">
      <h2>Blueberry Scone</h2>
    </div>
  </div>
</div>
<div class="CardContainer Half">
  <div class="Card FullBleed">
    <div class="CardThumbnail" style="background-image: url(http://www.starbucks.com/assets/d3759473a89746f49358a63aab116ca1.jpg)"></div>
      <div class="CardContents">
      <h2>Butter Croissant</h2>
    </div>
  </div>
</div>
<div class="CardContainer Half">
  <div class="Card FullBleed">
    <div class="CardThumbnail" style="background-image: url(http://www.starbucks.com/assets/1237d5dc538b4292804dde65a617d438.jpg)"></div>
      <div class="CardContents">
      <h2>Butterfly Cookie</h2>
    </div>
  </div>
</div>
<div class="CardContainer Half">
  <div class="Card FullBleed">
   <div class="CardThumbnail" style="background-image: url(http://www.starbucks.com/assets/6505400432664393bb70554fe44ceec5.jpg)"></div>
      <div class="CardContents">
      <h2>Cheese Danish</h2>
    </div>
  </div>
</div>
<div class="CardContainer Half">
  <div class="Card FullBleed">
    <div class="CardThumbnail" style="background-image: url(http://www.starbucks.com/assets/01b876b249624bb1a3c67172e08fdcb6.jpg)"></div>
      <div class="CardContents">
      <h2>Chewy Chocolate Cookie</h2>
    </div>
  </div>
</div>
<div class="CardContainer Half">
  <div class="Card FullBleed">
    <div class="CardThumbnail" style="background-image: url(http://www.starbucks.com/assets/725f951a16364258ba5d4b09fd5ae366.jpg)"></div>
      <div class="CardContents">
      <h2>Chocolate Chip Cookie</h2>
    </div>
  </div>
</div>
<div class="CardContainer Half">
  <div class="Card FullBleed">
    <div class="CardThumbnail" style="background-image: url(http://www.starbucks.com/assets/bff24e5a7b924520ae515d0c82d0a32e.jpg)"></div>
      <div class="CardContents">
      <h2>Chocolate Croissant</h2>
    </div>
  </div>
</div>
<div class="CardContainer Half">
  <div class="Card FullBleed">
    <div class="CardThumbnail" style="background-image: url(http://www.starbucks.com/assets/39a227d0275f4792a103292b251fb7f1.jpg)"></div>
      <div class="CardContents">
      <h2>Cinnamon Morning Bun</h2>
    </div>
  </div>
</div>
<div class="CardContainer Half">
  <div class="Card FullBleed">
    <div class="CardThumbnail" style="background-image: url(http://www.starbucks.com/assets/d0bf434bb1e845fe837a03cca587d5dc.jpg)"></div>
      <div class="CardContents">
      <h2>Classic Coffee Cake</h2>
    </div>
  </div>
</div>
<div class="CardContainer Half">
  <div class="Card FullBleed">
    <div class="CardThumbnail" style="background-image: url(http://www.starbucks.com/assets/b8785679e4c74e148ba88025c36c9cb8.jpg)"></div>
      <div class="CardContents">
      <h2>Cranberry Orange Scone</h2>
    </div>
  </div>
</div>
<div class="CardContainer Half">
  <div class="Card FullBleed">
    <div class="CardThumbnail" style="background-image: url(http://www.starbucks.com/assets/2131d46ec4cf4c50881b112b4e44dd24.jpg)"></div>
      <div class="CardContents">
      <h2>Devil's Food Doughnut</h2>
    </div>
  </div>
</div>
<div class="CardContainer Half">
  <div class="Card FullBleed">
    <div class="CardThumbnail" style="background-image: url(http://www.starbucks.com/assets/2e619840756f489c9f31893d52449a80.jpg)"></div>
      <div class="CardContents">
      <h2>Double Chocolate Brownie</h2>
    </div>
  </div>
</div>
<div class="CardContainer Half">
  <div class="Card FullBleed">
    <div class="CardThumbnail" style="background-image: url(http://www.starbucks.com/assets/2caf8e7fc07f4f968439d7917b98d02b.jpg)"></div>
      <div class="CardContents">
      <h2>Double Chocolate Chunk Brownie</h2>
    </div>
  </div>
</div>
  

<div class="FooterPlaceholder">
  <p>Footer Content</p>
  <p>© 2016 Starbucks Corporation. All rights reserved.</p>
</div>

</section>

<?php include("footer.php"); ?>

