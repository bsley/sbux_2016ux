<header>
  <a class="Logo" id="NavHome"></a>
  <nav class="NavContainer">
    <ul class="PrimaryNav">
      <li><a class="GetNotification" data-notif="Sorry. Order is not included in this prototype.">Order</a></li>
      <li><a id="NavCards">Cards</a></li>
      <li><a id="NavHistory">History</a></li>
      <li><a id="NavStores">Stores</a></li>
    </ul>
    <ul class="SecondaryNav">
      <li><a href="http://store.starbucks.com" target="_blank">Shop</a></li>
      <li><a href="">Gift</a></li>
      <li><a href="http://company.starbucks.com/" target="_blank">Company</a></li>
      <li><span>|</span></li>
<!--       <li><a id="NavSettings">Settings</a></li>
 -->      <li><a id="NavSignOut">Sign Out</a></li>
    </ul>
  </nav>
</header>

<div class="Search">
  <input placeholder="Search Anything">
</div>