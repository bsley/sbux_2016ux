<div class="CardContainer">
    <div class="Card FullBleed">
      <div class="CardContents">
        <h2>The Best Coffee. Starbucks Coffee Finder.</h2>
        <p>Our coffee masters have distilled their years of tasting knowledge down to three simple questions to help you find a Starbucks coffee you’re sure to love.</p>
        <button class="Button">Explore Coffee</button>
      </div>
    </div>
  </div>
  <div class="CardContainer">
    <div class="Card FullBleed" id="Card_Chorizo" style="background-image: url(_img/bg-reserve.jpg)">
      <div class="CardContents Negative">
        <h2>Our rarest coffees, small-batch roasted in Seattle.</h2>
        <button class="Button Negative">Learn More</button>
      </div>
    </div>
  </div>
  <div class="CardContainer">
    <div class="Card FullBleed Large">
      <div class="CardThumbnail" style="background-image: url(_img/sula.jpg)"></div>
      <div class="CardContents">
        <h2>Rich coffee tradition produces rare Indonesian coffee</h2>
        <p>The sandy coast of Sulawesi, Indonesia is a vision of pristine beaches.</p>
        <button class="Button">Explore</button>
      </div>
    </div>
  </div>

<div class="FooterPlaceholder">
  <p>Footer Content</p>
  <p>© 2016 Starbucks Corporation. All rights reserved.</p>
</div>