<?php include("header.php"); ?>
<?php include("nav_signedin.php"); ?>

<section class="PrimaryCrate" style="background-image: url(_img/bacon.png); background-color: #0e0e0e">
  <div class="PrimaryCrateInner FoodDetail">
    <br><Br><br><Br><br><Br><p>Food > Bakery > Chonga Bagel</p>
    <br><Br>
    <h1>Chonga Bagel</h1>
    <p>A bagel topped with Cheddar cheese, poppy seeds, sesame seeds, onion and garlic.</p>
    <button class="Button Negative">Order Now</button>
  
  </div>
</section>
<section class="SecondaryCrate">

<div class="CardContainer Half">
  <div class="Card linked FullBleed">
    <div class="CardContents">
      <h2>Nutritional Information</h2>
      
    </div>
    <img src="_img/nutfacts.png">
  </div>
</div>
<div class="CardContainer Half">
  <div class="Card linked FullBleed">
    <div class="CardContents">
      <h2>Ingredients</h2>
      <p><b>enriched flour</b> (wheat flour, niacin, reduced iron, thiamin mononitrate, riboflavin, folic acid)<b>, water, cheddar cheese</b> (pasteurized milk, cheese cultures, salt, enzymes, annatto [color])<b>, sugar, poppy seeds, sesame seeds, dried minced onion, yeast, salt, dried minced garlic, vital wheat gluten, malt</b> (malted barley flour, wheat flour, dextrose)<b>, canola oil, calcium propionate, dough conditioner</b> (wheat flour, calcium sulfate, enzymes), vinegar. contains: wheat, milk.</p>
      <p>This tasty bagel with a quirky name is back by popular demand. Yes, “Chonga” may sound odd at first, but it’s not a dance people do at parties. It’s simply shorthand for CHeddar-ONion-GArlic, three savory toppings that heighten the flavor of this moist and chewy roll. You’ll find tasty poppy and sesame seeds on there too – but we stopped at Chonga because “Chongapose Bagel” was pushing it.</p>
      <p><em>Only available in the Pacific Northwest. The nutritional information is for the bagel by itself and does not include optional cream cheese.</em></p>
    </div>
  </div>
</div>
  
  
</section>

<?php include("footer.php"); ?>

