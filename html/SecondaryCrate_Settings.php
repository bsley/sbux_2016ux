<div class="CardsPanel First">
	  <div class="CardContainer">
	  	<!-- <label class="CardLabel">Personal Information</label> -->
	    <div class="Card SettingsCard">
	      <div class="CardContents ">
	      	<p class="CardTitle">Personal Information</p>
			<p>
				Jeremy P. Beasley<br>
				432 Marionberry Ave S.<br>
				Seattle, WA 98120<br>
			</p>
			<p>Partner Number #12302342</p>
	        <button class="Button">Edit</button>
	      </div>
	    </div>
	  </div>
	  <div class="CardContainer">
	  	<!-- <label class="CardLabel">Password</label> -->
	    <div class="Card SettingsCard">
	      <div class="CardContents ">
		    <p class="CardTitle">Account Information</p>
		    	<p>jeremy@bsley.com</p>
		    	<p>•••••••••</p>
	        <button class="Button">Edit Email</button>
	        <button class="Button">Change Password</button>
	      </div>
	    </div>
	  </div>
	  <div class="CardContainer">
	  	<!-- <label class="CardLabel">Payment Methods</label> -->
	    <div class="Card SettingsCard">
	      <div class="CardContents ">
	      	<p class="CardTitle">Payment Methods</p>
	        <p>Visa (#5648)</p>
	        <button class="Button">Edit</button>
	        <br><br>
	        <p>Visa (#4332)</p>
	        <button class="Button">Edit</button>
	      </div>
	    </div>
	  </div>
	  <div class="CardContainer">
	  	<!-- <label class="CardLabel">Connected Apps</label>
 -->	    <div class="Card SettingsCard">
	      <div class="CardContents ">
		      <p class="CardTitle">Connected Apps</p>
	        <p>Twitter</p>
	        <button class="Button">Disconnect</button>
	        <br><br>
	        <div id="FacebookConnect">
		        <p>Facebook</p>
		        <button class="Button GetNotification DisconnectFacebook" data-notif="Your Facebook account has been disconnected.">Disconnect</button>
	      	</div>
	      </div>
	    </div>
	  </div>
	  <div class="CardContainer">
	  	<!-- <label class="CardLabel">Preferences</label> -->
	    <div class="Card SettingsCard">
	      <div class="CardContents ">
	      	<p class="CardTitle">Preferences</p>
	        <p>What?</p>
	        <button class="Button">Edit</button>
	      </div>
	    </div>
	  </div>
	  <br><br><br>
	</div>
	<div class="CardsPanel Second">
		<div class="CardContainer">
		<label><a class="NavLinkBack">‹ Back</a></label><br><Br>
	    <div class="Card SettingsCard">
	      <div class="CardContents ">
	        <p>History Detail / Receipt</p>
	        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
	      </div>
	    </div>
	  </div>
	</div>
