<?php include("header.php"); ?>
<?php include("nav_signedin.php"); ?>

<section class="PrimaryCrate">
  <div class="PrimaryCrateInner AccountSummary">
  <h1>Order</h1>
    <!-- Account Navigation & Star Balance -->
  </div>
</section>
<section class="SecondaryCrate">
  <div class="CardContainer Half">
  <label class="CardLabel">Recent / Featured Items</label>
    <div class="Card linked FullBleed OneTwo" style="background-imagez: url(http://food.starbucks.com.s3-website-us-east-1.amazonaws.com/assets/img/menu/breakfast-m.jpg)">
      <div class="CardContents Negative">
        <h2><a href="food_bakery.php">Hot Breakfast</a></h2>
        <!-- <button class="Button Negative">Learn More</button> -->
      </div>
    </div>
  </div>
  <div class="CardContainer Half">
    <div class="Card linked FullBleed OneTwo" style="background-imagez: url(http://food.starbucks.com.s3-website-us-east-1.amazonaws.com/assets/img/menu/bistro-boxes.jpg)">
      <div class="CardContents Negative">
        <h2><a href="food_bakery.php">Bistro Boxes</a></h2>
        <!-- <button class="Button Negative">Learn More</button> -->
      </div>
    </div>
  </div>
  <div class="CardContainer Half">
    <label class="CardLabel">Categories</label>
    <div class="Card linked FullBleed OneTwo" style="background-imagez: url(http://food.starbucks.com.s3-website-us-east-1.amazonaws.com/assets/img/menu/bakery.jpg)">
      <div class="CardContents Negative">
        <h2><a href="food_bakery.php">Bakery</a></h2>
        <!-- <button class="Button Negative">Learn More</button> -->
      </div>
    </div>
  </div>
<div class="CardContainer Half">
    <div class="Card linked FullBleed OneTwo" style="background-imagez: url(http://food.starbucks.com.s3-website-us-east-1.amazonaws.com/assets/img/menu/sandwiches.jpg)">
      <div class="CardContents Negative">
        <h2><a href="food_bakery.php">Sandwiches</a></h2>
        <!-- <button class="Button Negative">Learn More</button> -->
      </div>
    </div>
  </div>
<div class="CardContainer Half">
    <div class="Card linked FullBleed OneTwo" style="background-imagez: url(http://food.starbucks.com.s3-website-us-east-1.amazonaws.com/assets/img/menu/salads.jpg)">
      <div class="CardContents Negative">
        <h2><a href="food_bakery.php">Salads</a></h2>
        <!-- <button class="Button Negative">Learn More</button> -->
      </div>
    </div>
  </div>
<div class="CardContainer Half">
  <div class="Card linked FullBleed OneTwo" style="background-imagez: url(http://food.starbucks.com.s3-website-us-east-1.amazonaws.com/assets/img/menu/yogurt.jpg)">
    <div class="CardContents Negative">
      <h2><a href="food_bakery.php">Yogurt & Fruit</a></h2>
      <!-- <button class="Button Negative">Learn More</button> -->
    </div>
  </div>
</div>
<div class="CardContainer Half">
  <div class="Card linked FullBleed OneTwo" style="background-imagez: url(http://food.starbucks.com.s3-website-us-east-1.amazonaws.com/assets/img/menu/small-plates.jpg)">
    <div class="CardContents Negative">
      <h2><a href="food_bakery.php">Starbucks Evenings</a></h2>
      <!-- <button class="Button Negative">Learn More</button> -->
    </div>
  </div>
</div>
  

<div class="FooterPlaceholder">
  <p>Footer Content</p>
  <p>© 2016 Starbucks Corporation. All rights reserved.</p>
</div>

</section>

<?php include("footer.php"); ?>

