<!DOCTYPE html>
<html>
<head>
   <title>Starbucks</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <link href="master.css" rel="stylesheet">
  <script src="master.js"></script>
</head>
<body class="Unselectable">
<!-- <div id="LayoutToggle"></div>-->
<div class="NotificationGlobal">
	<p>
		<span>&#10003;</span>
		<em class="NotificationText">This is a notification!</em>
	</p>
</div>


