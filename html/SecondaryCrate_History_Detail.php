<li class="receipt has-alcohol">
  <button class="close-receipt"></button>
  <!-- header -->
  <div class="receipt-top bottom-dotted">
    <div class="receipt-head">
      
      <div class="date-time">January 4th, 2016 at 08:21PM</div>
      <div class="check-number">Check #797911</div>
    </div>
    <div class="receipt-location">
      <p class="dark-text">Starbucks (Pioneer Square)</p>
      <p class="dark-text">1124 Occidental Ave.</p>
      <p class="dark-text">Seattle, WA 98101</p>
    </div>
    <div>
      <p><span class="star-text">23.2</span><span class="star-outline">☆</span> Stars Earned</p>
      <p class="expired-text">Stars Expire August 1st, 2016</p>
    </div>
  </div>
  <!-- /end header -->
  <!-- itemized -->
  <div class="itemized-list bottom-dotted">
    <div class="itemized-list-item">
      <div class="left-column">
        <p class="item-name">Spanish Coffee *</p>
        <p class="item-size">Brugal Spiced Rum</p>
        <p class="item-size">2 Shots added</p>
        <p class="item-size">Bailey's Irish Coffee Creamer
      </div>
      <div class="right-column">$14.00</div>
    </div>
    <div class="itemized-list-item">
      <div class="left-column">
        <p class="item-name">Banana Nut Bread</p>
        <p class="item-size">Warmed</p>
      </div>
      <div class="right-column">$10.75</div>
    </div>
    <div class="itemized-list-item">
      <div class="left-column">
        <p class="item-name">Grande Caffe Latte</p>
      </div>
      <div class="right-column">$5.00</div>
    </div>
  </div>
  <div class="receipt-footer bottom-dotted">
    <div class="receipt-tax discount">
      <div class="left-column">Discount</div>
      <div class="right-column">-$1.81</div>
    </div>
    <div class="receipt-tax">
      <div class="left-column">Tax</div>
      <div class="right-column">$0.40</div>
    </div>
    <div class="towards-stars receipt-tax">
      <div class="left-column">
        <p class="gold-text">Total applied toward Stars</p>
      </div>
      <div class="right-column">
        <p class="gold-text">$10.27</p>
      </div>
    </div>
  </div>
  <div class="order-total">
    <div class="receipt-tax">
      <div class="left-column">Order Total</div>
      <div class="right-column">
        <div class="dark-text">$10.67</div>
      </div>
    </div>
  </div>
  
  <!-- /end itemized -->
</li>