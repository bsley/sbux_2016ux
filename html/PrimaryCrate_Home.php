<div class="PrimaryCrateInner AccountSummary" id="DreamingInGreen">
    <div class="AccountGreeting HiddenOnMobile">
      <h4>Good Morning, Sarah.</h4>
      <!-- <div class="AccountDecision">
        <p>It looks like your primary card balance is low. Would you like to reload it?</p>
        <a href="cards.php"><button class="Button Negative">Yes</button></a>
        <button class="Button Negative" id="RemindMeLater">Remind Me Later</button>
      </div> -->
    </div>
    <!-- Star Guage -->
    <div class="AccountStars">
      <div class="StarBalance">229</div>
      <div class="StarGuage" data-progress="0">
        <div class="circle">
          <div class="mask full">
            <div class="fill"></div>
          </div>
          <div class="mask half">
            <div class="fill"></div>
            <div class="fill fix"></div>
          </div>
        </div>
        <div class="inset">
          <div class="svg-star">
            <svg class="round-star" viewBox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <title></title>
                <desc></desc>
                <defs></defs>
                <g>
                  <polygon x="0" points="50,9 60.5,39.5 92.7,40.1 67,59.5 76.4,90.3 50,71.9 23.6,90.3 32.9,59.5 7.2,40.1 39.4,39.5" />
                </g>
              </svg>
          </div>
        </div>
      </div>
    </div><!-- /end Star Guage -->
    <div class="AccountNotification">
      <span></span>
      <p>You have enough Stars for multiple Rewards</p>
    </div>
    <!-- <div class="StatusProgress">
      <p>Earn 198 stars by Feb 23, 2016 to retain Gold</p>
    </div> -->
    <div class="ExpiringStars HiddenOnMobile">
      <div class="ExpiringStarLabel">Expiring Stars</div>
      <ul>
        <li>
          <span class="ExpDate">August 1st</span>
          <div class="StarIcon"></div>
          <span class="ExpAmount">10.2</span>
        </li>
        <li>
          <span class="ExpDate">September 1st</span>
          <div class="StarIcon"></div>
          <span class="ExpAmount">12.3</span>
        </li>
        <li>
          <span class="ExpDate">October 1st</span>
          <div class="StarIcon"></div>
          <span class="ExpAmount">29.1</span>
        </li>
        <li>
          <span class="ExpDate">November 1st</span>
          <div class="StarIcon"></div>
          <span class="ExpAmount">54.3</span>
        </li>
        <li>
          <span class="ExpDate">December 1st</span>
          <div class="StarIcon"></div>
          <span class="ExpAmount">23.7</span>
        </li>
        <li>
          <span class="ExpDate">January 1st</span>
          <div class="StarIcon"></div>
          <span class="ExpAmount">2.4</span>
        </li>
      </ul>
    </div>

  </div>
<script type="text/javascript">

  $(document).ready(function(){
    setTimeout(function(){
      $('.StarGuage').attr('data-progress', 84);
    }, 300);
  });

</script>
