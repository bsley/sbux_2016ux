<div class="CardsPanel First">
		<!-- <div class="CardContainer">
			<Br>
			<button class="Button">+ Add A New Card</button>
		</div> -->
		<div class="CardContainer">
	    <div class="Card HistoryItem">
	      <div class="CardContents ">
        	<div class="CardTitle">
        		<p><em id="PrimaryCardName">&#10003; My Primary Card</em> (#2453)</p>
        		<p><em id="PrimaryCardBalance">42.54</em> <span>as of 3s ago</span></p>
        	</div>
        	<div class="CardArt">
        		<img src="_img/card_01.png" />
        	</div>
        	<div class="CardActions">
	        	<button class="Button Primary">Pay</button>
	        	<button class="ReloadButton Button">Reload</button>
	        	<button class="TransferBalanceButton Button SliderForward">Transfer Balance</button>
	        	<button class="Button">Remove</button>
        	</div>
        </div>
	    </div>
	  </div>
	  <div class="CardContainer" id="SecondaryCard">
	    <div class="Card HistoryItem">
	      <div class="CardContents">
        	<div class="CardTitle">
        		<p><em id="SecondaryCardName">My Card</em> (#6534)</p>
        		<p><em id="SecondaryCardBalance">4.89</em> <span>as of 3s ago</span></p>
        	</div>
        	<div class="CardArt">
        		<img src="_img/card_02.png" />
        	</div>
        	<div class="CardActions">
	        	<button class="Button Primary">Pay</button>
	        	<button class="ReloadButton Button">Reload</button>
	        	<button class="TransferBalanceButton Button SliderForward">Transfer Balance</button>
	        	<button class="Button GetNotification MakeCardPrimary" data-notif="This card is now your primary card!">Make Card Primary</button>
        		<button class="Button RemoveACard GetNotification" data-notif="Your Card (#6534) has been removed from this account.">Remove</button>
        	</div>
        </div>
	    </div>
	  </div>
	</div>
	<!-- SECOND PANEL -->
	<div class="TransferBalancePanel CardsPanel Second">
		<div class="CardContainer">
			<div class="Card TransferTool">
				<div class="CardContents ">
					<div class="CardTitle">
		        <p>Transfer Balance</p>
		        <p class="SliderBackward CardClose">×</p>
	        </div>
					<div class="TransferFrom">
						<p class="TransferOrigin">From</p>
						<div class="TransferCard Unselectable">
							<div class="TransferCardArt">
								<img src="_img/card_02.png" />
							</div>
							<div class="TransferCardTitle">My Card (#6534)</div>
							<div class="TransferCardBalance">$4.89</div>
							<div class="TransferCardExpand"></div>
						</div>
					</div>
					<div class="TransferFrom">
						<p class="TransferOrigin">To</p>
						<div class="TransferCard Unselectable">
							<div class="TransferCardArt">
								<img src="_img/card_01.png" />
							</div>
							<div class="TransferCardTitle">&#10003; My Primary Card (#)</div>
							<div class="TransferCardBalance">$42.54</div>
							<div class="TransferCardExpand"></div>
						</div>
					</div>
					<div class="TransferAmount">
						<p>Amount</p>
						<div class="TransferAmountField">
							<input placeholder="$0.00">
						</div>
					</div>
				</div>
			</div>
	    <br><br>
	    <div class="CardTitle line cardControlButtons">
	    	<p><button class="Button SliderBackward GetNotification MakeATransfer" data-notif="$4.89 has been transfered to your Primary Card (#6534).">Complete Transfer</button></p>
	    	<p><a class="SliderBackward">Cancel</a></p>
	    </div>
	  </div>
	</div>
  <!-- SECOND PANEL -->
  <div class="ReloadPanel CardsPanel Second">
    <div class="CardContainer">
      <div class="Card" style="margin-bottom: 20px;">
        <div class="CardContents">
          <div class="CardTitle">
            <p>Reload Primary Card ending in (#2453)</p>
          </div>
          <!-- <div class="ReloadInformation">Reload Information</div> -->
          <div class="ReloadAmountOptions">
            <div class="SetAmounts">
                <input type="radio" name="reloadAmount" value="10.00" id="amount10" />
                <label for="amount10">$10</label>
                <input type="radio" name="reloadAmount" value="25.00" id="amount25" />
                <label for="amount25">$25</label>
                <input type="radio" name="reloadAmount" value="50.00" id="amount50" />
                <label for="amount50">$50</label>
                <input type="radio" name="reloadAmount" value="100.00" id="amount100" />
                <label for="amount100">$100</label>
            </div>
          </div>
          <div class="AutoReloadOptions">
            <div class="AutoReloadOption">
              <input type="radio" name="autoReloadOptions" value="ReloadOnce" id="ReloadOnce"  class="GetNotification" data-notif="This will only reload your card this one time." />
              <label for="ReloadOnce">Reload Once </label>
            </div>
            <div class="AutoReloadOption">
              <input type="radio" name="autoReloadOptions" value="AutoReloadOn" id="AutoReloadOn" class="GetNotification" data-notif="This will reload whenever the card drops below the set balance." />
              <label for="AutoReloadOn">Auto-Reload...</label>
              <div class="AutoReloadOnOptions">
                <div class="desiredAmount line">Reload the above chosen amount every time the balance drops below:</div>
                <input type="number" placeholder="" pattern="[0-9]*" />
              </div>
            </div>
          </div>
        </div> <!-- end leftcolumn -->
        <!-- right column -->
        <div class="CardContents">
          <div class="CardTitle">
            <p class="detailTitle">Choose Payment Method</p>
            <ul class="PaymentMethodOptions">
              <li>
                <input type="radio" id="payWithCreditOrDebit" name="paymentMethodOptions" value="payWithCreditOrDebit" class="GetNotification" data-notif="Payment Method changed to Credit Card" />
                <label for="payWithCreditOrDebit" class="line">
                Saved Credit/Debit Card
                  <!-- <img src="https://libra-testusen.starbucks.com/static/images/shop/payment_card_credit_debit.png" /> -->
                </label>
                <!-- Card Details -->
                <div id="cardDetails" class="line">
                  <!-- Edit Card Info -->
                  <div class="editCardInfo inputFields line">
                    <label for="nameCard">Name this card</label>
                    <input disabled="true" id="nameCard" type="text" value="Visa: My Test Card" />
                    <label for="cardNumber">Enter your card number</label>
                    <input disabled="true" id="cardNumber" type="text" value="Card Number: •••• 1111" />
                    <label for="expirationDate">Enter expiration date</label>
                    <input disabled="true" id="expirationDate" type="text" value="Expiration Date: 12/2020" />
                    <br /><br />
                    <button id="editCard"  class="Button">edit card</button>
                    <button id="saveCardInfo"  class="hidden Button GetNotification" data-notif="Credit card information has been saved.">save card</button>
                  </div><!-- /end Edit Card Info -->
                  <!-- Billing Address -->
                  <div id="paymentAddress lastUnit">
                    <div class="billingTitle">Billing Address</div>
                    <div class="editBillingInfo inputFields">
                      <label for="firstName">First name</label>
                      <input disabled="true" id="firstName" type="text" value="Hoover" />
                      <label for="lastName">Last name</label>
                      <input disabled="true" id="lastName" type="text" value="Ville" />
                      <label for="streetAddress">Street address</label>
                      <input disabled="true" id="streetAddress" type="text" value="1721 1st Ave S" />
                      <label for="cityName">City</label>
                      <input disabled="true" id="cityName" type="text" value="Seattle" />
                      <label for="stateName">State</label>
                      <input disabled="true" id="stateName" type="text" value="WA" />
                      <label for="zipCode">Zip code</label>
                      <input disabled="true" id="zipCode" type="text" value="98134-1403" />
                      <br /><br />
                      <button id="editBilling" class="Button">edit billing address</button>
                      <button id="saveBillingInfo" class="hidden Button GetNotification" data-notif="Billing Address Updated">save billing address</button>
                    </div>
                  </div><!-- /end Billing Address -->
                </div><!-- /end Card Details -->
              </li>
              <li>
                <input type="radio" id="payWithVisaCheckout" name="paymentMethodOptions" value="payWithVisaCheckout" class="GetNotification" data-notif="Payment Method changed to Visa Checkout" />
                <label for="payWithVisaCheckout">
                  Visa Checkout
                  <!-- <img src="https://assets.secure.checkout.visa.com/VmeCardArts/partner/POS_horizontal_99x34.png" /> -->
                </label>
              </li>
              <li>
                <input type="radio" id="payWithPal" name="paymentMethodOptions" value="payWithPayPal" class="GetNotification" data-notif="Payment Method changed to PayPal" />
                <label for="payWithPal" >
                  PayPal
                  <!-- <img src="https://libra-testusen.starbucks.com/static/images/shop/payment_paypal.png" /> -->
                </label>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="CardTitle">
        <p><button disabled class="Button SliderBackward ReloadCard" data-notif="">Reload Now</button></p>
        <p><button class="Button SliderBackward GetNotification CancelReload" data-notif="Reload cancelled!">Cancel</a></button>
      </div>
      <span>Amounts shown in U.S. Dollars</span>
    </div>
  </div>
