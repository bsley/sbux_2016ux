<div class="CardContainer" >
	<div class="Card">
	  <div class="CardContents">
	  	<h2>I have a Starbucks account.</h2>
	  	<input class="SignInField" placeholder="jeremy@bsley.com">
	  	<input class="SignInField" placeholder="••••••••••••">
			<div id="Username_Help_Question" class="username_help_question">
			Not sure if you have a <a href="#Username_Help_Answer" aria-label="Click to learn whether you login with a username or email" class="">username</a>?
			</div>
			<div id="Username_Help_Answer" class="username_help_answer expandable_details" aria-expanded="false" role="region" tabindex="-1" style="display: none;">
			If you created your Starbucks account after October 2012 using the iOS app, or after January 2013 using the web site, log in with your email address. For older accounts,&nbsp;log in with your username.
			</div>
			Forgot your <a aria-label="Retrieve your username" href="/account/forgot-username?AllowGuest=False" id="AT_SignIn_ForgotUser">username</a> or <a aria-label="Retrieve your password" href="/account/forgot-password?AllowGuest=False" id="AT_SignIn_ForgotPass">password</a>?<Br><Br>
	  	<button class="Button" id="NavSignInSubmit">Sign In</button>
	  </div>
	</div>
</div>
<div class="CardContainer">
  <div class="Card FullBleed">
    <div class="CardContents">
      <h2>Create an account and bring on the rewards!</h2>
      <p>Join Starbucks Rewards to earn free food and drinks, get free refills, pay and order with your phone, and more.</p>
      <button class="Button GetNotification" data-notif="Sorry. This flow is not included in this prototype.">Join Now</button>
    </div>
  </div>
</div>